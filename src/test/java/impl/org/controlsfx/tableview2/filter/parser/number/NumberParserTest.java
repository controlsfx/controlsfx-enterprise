package impl.org.controlsfx.tableview2.filter.parser.number;

import org.junit.Assert;
import org.junit.Test;

public class NumberParserTest {

    // =
    @Test
    public void isEquals() {
        Assert.assertTrue(new NumberParser<>().parse("= 100").test(100));
    }

    @Test
    public void isEqualsFalse() {
        Assert.assertFalse(new NumberParser<>().parse("= 100").test(110));
    }

    // ≠
    @Test
    public void isNotEquals() {
        Assert.assertTrue(new NumberParser<>().parse("≠ 100").test(110));
    }

    @Test
    public void isNotEqualsFalse() {
        Assert.assertFalse(new NumberParser<>().parse("≠ 100").test(100));
    }

    // <
    @Test
    public void isLessThan() {
        Assert.assertTrue(new NumberParser<>().parse("< 100").test(99));
    }

    @Test
    public void isNotLessThan() {
        Assert.assertFalse(new NumberParser<>().parse("< 100").test(100));
    }

    // ≤
    @Test
    public void isLessThanEquals1() {
        Assert.assertTrue(new NumberParser<>().parse("≤ 100").test(100));
    }

    @Test
    public void isLessThanEquals2() {
        Assert.assertTrue(new NumberParser<>().parse("≤ 100").test(99));
    }

    @Test
    public void isNotLessThanEquals() {
        Assert.assertFalse(new NumberParser<>().parse("≤ 100").test(101));
    }

    // >
    @Test
    public void isGreaterThan() {
        Assert.assertTrue(new NumberParser<>().parse("> 100").test(101));
    }

    @Test
    public void isNotGreaterThan() {
        Assert.assertFalse(new NumberParser<>().parse("> 100").test(90));
    }

    // ≥
    @Test
    public void isGreaterThanEquals1() {
        Assert.assertTrue(new NumberParser<>().parse("≥ 100").test(100));
    }

    @Test
    public void isGreaterThanEquals2() {
        Assert.assertTrue(new NumberParser<>().parse("≥ 100").test(101));
    }

    @Test
    public void isNotGreaterThanEquals() {
        Assert.assertFalse(new NumberParser<>().parse("≥ 100").test(90));
    }

    @Test
    public void caseNull() {
        Assert.assertFalse(new NumberParser<>().parse("≥ 100").test(null));
    }
    
    // OR in numbers
    @Test
    public void OrFirst() {
        Assert.assertTrue(new NumberParser<>().parse("= 150 OR = 160 AND = 100 ").test(150));
    }

    // AND in numbers
    @Test
    public void AndFirst() {
        Assert.assertFalse(new NumberParser<>().parse("= 150 AND = 160 OR = 100 ").test(150));
    }
}
